from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from PIL import Image
import math
from io import BytesIO
import time
import urllib
import requests
import json
import re
import cv2 as cv


# Set Eager API
tf.enable_eager_execution()
tfe = tf.contrib.eager

TRAIN_WEIGHT_PATH = "../model/192weights"
load_weight_path = "../model/weights24+0.0001#512-512-512#100.h5"  # change the name of your trainning from the folder model
TRAIN_DATA_PATH = '../dataset/239_train_data.txt'  # change to the name of train_data in your dataset folder
TRAIN_LABES_PATH = '../dataset/239_train_labes.txt'    #change to the name of train_label in your dataset folder
TEST_DATA_PATH = '../dataset/51_test_data.txt'
TEST_LABES_PAHT = '../dataset/51_test_labes.txt'
FIGURE_SAVE_PATH = '../figureSave/'
PREFIX_SAVE_IMAGE_PATH = '../predict/'
DATA_PREDICTION_JSON_PATH = '../predict_data_text/'

human_names = ['Hien Duong','Ba Bui','Dang Hoang']


image_width = 56
image_height = 56
num_images = 187
# Parameters
learning_rate = 0.0001
num_steps = 1000
batch_size = 32    # numbers of training images for a step
display_step = 100

# Network Parameters
strLayer = (load_weight_path.split('#')[1]).split('-')
n_hidden_1 = 512 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 512 # 3nd layer number of neurons
num_input = 9408 # MNIST data input (img shape: 64*128)
num_classes = 14 # MNIST total classes (0-4)

#load_weight_path = re.sub(["n_hidden_1","n_hidden_2","n_hidden_3"],[n_hidden_1,n_hidden_2,n_hidden_3],load_weight_path)

# Define the neural network. To use eager API and tf.layers API together,
# we must instantiate a tfe.Network class as follow:
class MNISTModel(tf.keras.Model):
  def __init__(self):
    super(MNISTModel, self).__init__()
    self.dense1 = tf.keras.layers.Dense(units = n_hidden_1,activation = tf.nn.relu)
    self.dense2 = tf.keras.layers.Dense(units = n_hidden_2,activation = tf.nn.relu)
    self.dense3 = tf.keras.layers.Dense(units = n_hidden_3,activation = tf.nn.relu)
    self.denseout = tf.keras.layers.Dense(units = num_classes)

  def call(self, input):
    """Run the model."""
    result = self.dense1(input)
    result = self.dense2(result)
    result = self.dense3(result)
    result = self.denseout(result)  # reuse variables from dense2 layer
    return result

def modelLearn():
    model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
        keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
        keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
    return model







# Cross-Entropy loss function
def loss_fn(inference_fn, inputs, labels):
    # Using sparse_softmax cross entropy
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=inference_fn(inputs), labels=labels))

# Calculate accuracy
def accuracy_fn(inference_fn, inputs, labels):
    prediction = tf.nn.softmax(inference_fn(inputs))
    #print(prediction)
    correct_pred = tf.equal(tf.argmax(prediction, 1), labels)
    return tf.reduce_mean(tf.cast(correct_pred, tf.float32))

def get_prediction(inference_fn, inputs, labels):
    return tf.nn.softmax(inference_fn(inputs))

#loadModel()

def get_traindata():
    train_images = np.loadtxt(TRAIN_DATA_PATH)
    #print(train_images.shape) 989 images (989, 64, 384)
    print(train_images.shape)
    num_ima = int(train_images.shape[0]/image_width)
    total_size = int(train_images.shape[1]*image_width) # 64*384 = 24576
    train_images = train_images.reshape((num_ima,total_size))
    #print(train_images.shape)
    train_labels = np.loadtxt(TRAIN_LABES_PATH)
    train_labels = train_labels.astype(int)
    return train_images,train_labels

def get_testdata():
    test_image = np.loadtxt(TEST_DATA_PATH)
    num_ima = int(test_image.shape[0]/image_width)
    total_size = int(test_image.shape[1]*image_width) # 64*384 = 24576
    testX = test_image.reshape((num_ima,total_size))
    test_labels = np.loadtxt(TEST_LABES_PAHT)
    testY = test_labels.astype(int)
    return testX, testY

def get_predictData():
    test_images = np.loadtxt(TEST_DATA_PATH)
    num_ima = int(test_images.shape[0]/image_width)
    test_images = test_images.reshape((num_ima,image_width,test_images.shape[1])).astype(int)
    test_labels = np.loadtxt(TEST_LABES_PAHT)
    test_labels = test_labels.astype(int)
    return test_images, test_labels

# Training
def training():
    rate = [0.0001]
    batch = [32,48]
    test_acuracy = 0;
    br = False
    for bat in batch:
        batch_size = bat
        for value_rate in rate:
            learning_rate = value_rate
            model = MNISTModel()

            train_images,train_labels = get_traindata()

            dataset = tf.data.Dataset.from_tensor_slices(
                (train_images,train_labels))
            dataset = dataset.repeat().batch(batch_size).prefetch(batch_size)
            dataset_iter = tfe.Iterator(dataset)
            # SGD Optimizer
            optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
            # Compute gradients
            grad = tfe.implicit_gradients(loss_fn)
            average_loss = 0.
            average_acc = 0.
            for step in range(num_steps):

                # Iterate through the dataset
                d = dataset_iter.next()

                # Images
                x_batch = d[0]
                # Labels
                y_batch = tf.cast(d[1], dtype=tf.int64)

                # Compute the batch loss
                batch_loss = loss_fn(model, x_batch, y_batch)
                average_loss += batch_loss
                # Compute the batch accuracy
                batch_accuracy = accuracy_fn(model, x_batch, y_batch)
                average_acc += batch_accuracy

                if step == 0:
                    # Display the initial cost, before optimizing
                    print("Initial loss= {:.9f}".format(average_loss))

                # Update the variables following gradients info
                optimizer.apply_gradients(grad(model, x_batch, y_batch))

                # Display info
                if (step + 1) % display_step == 0 or step == 0:
                    if step > 0:
                        average_loss /= display_step
                        average_acc /= display_step
                    print("Step:", '%04d' % (step + 1), " loss=",
                          "{:.9f}".format(average_loss), " accuracy=",
                          "{:.4f}".format(average_acc))
                    if(average_acc>=0.95):
                        load_weight_path = TRAIN_WEIGHT_PATH+str(batch_size)+"+"+str(learning_rate)+"#"+str(n_hidden_1)+"-"+str(n_hidden_2)+"-"+str(n_hidden_3)+"#"+str(int(average_acc*100))+".h5"
                        model.save_weights(load_weight_path)
                        #model.summary()
                    test_acuracy = average_acc
                    average_loss = 0.
                    average_acc = 0.
            if(test_acuracy > 0.95):
                br = True
        if(br):
            break

    #model.save_weights(TRAIN_WEIGHT_PATH)
    testX,testY = get_testdata()
    test_acc = accuracy_fn(model, testX, testY)
    print("Testset Accuracy: {:.4f}".format(test_acc))
    #model.summary()

#training()
def plot_image(i,predictions_array,true_label,img):
    predictions_array,true_label,img = predictions_array[i],true_label[i],img[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(img.reshape(image_width,image_height,3),cmap=plt.cm.binary)
    predicted_label = np.argmax(predictions_array)
    if predicted_label  == true_label:
        color = 'blue'
    else:
        color = 'red'
    plt.xlabel("{} {:2.0f}% ({})".format(human_names[predicted_label],
                    100*np.max(predictions_array),
                    human_names[int(true_label)]),
                    color = color)
def plot_value_array(i,predictions_array,true_label):
    predictions_array,true_label = predictions_array[i],true_label[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    thisplot = plt.bar(range(num_classes),predictions_array, color = "#777777")
    plt.ylim = ([0,1])
    predicted_label = int(np.argmax(predictions_array))
    #print(predicted_label)
    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')

def testing():
    #load model weights
    model = modelLearn()
    model.load_weights(load_weight_path)
    model.summary()
    model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

    #load data to predict
    test_images,test_labels = get_predictData()
    #Evaluate accuracy
    test_loss, test_acc  = model.evaluate(test_images,test_labels)

    print('Test accuracy:',test_acc)

    predictions = model.predict(test_images)

    print(predictions)

    #Plot the first X test images, their predicted label, and  the true label
    # Color correct predictions in blue, incorrect predictions in red

    num_rows = 5
    num_cols = 3
    num_images = num_rows*num_cols
    plt.figure(figsize=(2*2*num_cols,2*num_rows))

    for i in range(num_images):
    	plt.subplot(num_rows,2*num_cols,2*i+1)
    	plot_image(3+i,predictions,test_labels,test_images)
    	plt.subplot(num_rows,2*num_cols,2*i+2)
    	plot_value_array(3+i,predictions,test_labels)
    plt.show()

def resize_crop(image,size):
    img_format = image.format
    img = image.copy()
    old_size = img.size
    left = (old_size[0] - size[0]) / 2
    top = (old_size[1] - size[1]) / 2
    right = (old_size[0] - left)
    bottom = old_size[1] - top
    rect = [int(math.ceil(x)) for x in (left,top,right,bottom)]
    left, top, right, bottom = rect
    crop = image.crop((left,top,right,bottom))
    crop.format = img_format
    return crop


def resize_cover(image,size):
    img_format = image.format
    img = image.copy()
    img_size = img.size
    ratio = max(size[0] / img_size[0],size[1] / img_size[1])
    new_size = [int(math.ceil(img_size[0] * ratio)),int(math.ceil(img_size[1]*ratio))]
    img = img.resize((new_size[0],new_size[1]),Image.LANCZOS)
    img = resize_crop(img,size)
    img.format = img_format
    return img

def convertToRGB(img):
    return cv.cvtColor(img,cv.COLOR_BGR2RGB)

def getFacePredict(img_face,model):
    img = resize_cover(img_face,[56,56])
    array = np.array(img).reshape(56,56*3)
    array =  array.reshape((1,-1))
    prediction = model.predict(array)
    if(np.max(prediction)*100 > 90):
        index = int(np.argmax(prediction))
        return "%s %.0f"%(human_names[index],np.max(prediction)*100)
    else:
        return ""

def detect_faces(f_cascade,colored_img,model):
    img_copy = colored_img.copy()
    gray = cv.cvtColor(img_copy, cv.COLOR_BGR2GRAY)
    scaleFactor = 1.1
    faces = f_cascade.detectMultiScale(gray, scaleFactor, minNeighbors=5,minSize=(80, 80))
    print(faces)
    while len(faces) == 0:
        scaleFactor += 0.1
        faces = f_cascade.detectMultiScale(gray, scaleFactor, minNeighbors=5,minSize=(80, 80))
        if scaleFactor >= 2:
            break
        print(faces)

    arr = np.array(img_copy)
    for (x,y,w,h) in faces:
        cv.rectangle(img_copy,(x,y),(x+w,y+h),(0,255,0),2)
        print("x:%d, x+w:%d, y:%d, y+h: %d"%(x,x+w,y,y+h))
        arr_f = arr[y:y+h,x:x+w,:]
        #showImage(arr_f)
        img_face = Image.fromarray(convertToRGB(arr_f))
        face_name = getFacePredict(img_face,model)
        font = cv.FONT_HERSHEY_SIMPLEX
        cv.putText(img_copy,face_name,(x,y-2), font, 1,(0,15,250),2,cv.LINE_AA)
        #roi_gray = gray[y:y+h, x:x+w]
        #roi_color = colored_img[y:y+h, x:x+w]
        #eyes = eye_cascade.detectMultiScale(roi_gray)
        #for (ex,ey,ew,eh) in eyes:
        #    cv.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
    return img_copy

def predicting():
    img_path = "../img_train/phat_nguyen-05-02.jpg"
    #img_path = "../temp/dang-nhu-phat-dat-0.jpg"
    #load model weights
    model = modelLearn()
    model.load_weights(load_weight_path)
    #model.summary()
    model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

    haar_face_cascade = cv.CascadeClassifier('../data/haarcascade_frontalface_default.xml')
    imgArr = cv.imread(img_path)
    faces_detect_img = detect_faces(haar_face_cascade,imgArr,model)
    plt.figure()
    plt.imshow(convertToRGB(faces_detect_img))
    plt.grid(False)
    plt.show()

def main():
    training()
    #testing()
    #predicting()

if __name__ == '__main__':
    main()
