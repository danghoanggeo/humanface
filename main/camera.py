from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow import keras
import matplotlib.pyplot as plt
from PIL import Image
import math
from io import BytesIO
import time
import urllib
import requests
import json
import re
import cv2

face_cascade = cv2.CascadeClassifier('../data/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('../data/haarcascade_eye.xml')
load_weight_path = "../model/192weights32+0.0001#512-512-512#100.h5"
human_names = ['T Nguyen','Hien Duong','Ba Bui','T Vu','T Dat','P Nguyen','V Thoai','V Tuyen','Hong Cam','KHa','H Nhu','H Thy','Dang Hoang','Minh Huong']
n_hidden_1 = 512 # 1st layer number of neurons
n_hidden_2 = 512 # 2nd layer number of neurons
n_hidden_3 = 512 # 3nd layer number of neurons
num_classes = 14
num_input = 9408
image_width = 56
image_height = 56
num_images = 187
learning_rate = 0.0001

def modelLearn():
    model = keras.Sequential([keras.layers.Flatten(input_shape=(num_input,)),
        keras.layers.Dense(n_hidden_1,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_2,activation = tf.nn.relu),
        keras.layers.Dense(n_hidden_3,activation = tf.nn.relu),
        keras.layers.Dense(num_classes,activation=tf.nn.softmax)])
    return model

model = modelLearn()
model.load_weights(load_weight_path)
#model.summary()
model.compile(optimizer=tf.train.AdamOptimizer(learning_rate=learning_rate),loss='sparse_categorical_crossentropy',metrics=['accuracy'])

cap = cv2.VideoCapture(0)
cap.set(3, 640) #WIDTH
cap.set(4, 480) #HEIGHT

def resize_crop(image,size):
    img_format = image.format
    img = image.copy()
    old_size = img.size
    left = (old_size[0] - size[0]) / 2
    top = (old_size[1] - size[1]) / 2
    right = (old_size[0] - left)
    bottom = old_size[1] - top
    rect = [int(math.ceil(x)) for x in (left,top,right,bottom)]
    left, top, right, bottom = rect
    crop = image.crop((left,top,right,bottom))
    crop.format = img_format
    return crop


def resize_cover(image,size):
    img_format = image.format
    img = image.copy()
    img_size = img.size
    ratio = max(size[0] / img_size[0],size[1] / img_size[1])
    new_size = [int(math.ceil(img_size[0] * ratio)),int(math.ceil(img_size[1]*ratio))]
    img = img.resize((new_size[0],new_size[1]),Image.LANCZOS)
    img = resize_crop(img,size)
    img.format = img_format
    return img

def convertToRGB(img):
    return cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

def getFacePredict(img_face):
    #img_path = "../temp/dang-nhu-phat-dat-0.jpg"
    #load model weights
    img = resize_cover(img_face,[56,56])
    array = np.array(img).reshape(56,56*3)
    array =  array.reshape((1,-1))
    prediction = model.predict(array)
    if(np.max(prediction)*100 > 90):
        index = int(np.argmax(prediction))
        return "%s %.0f %%"%(human_names[index],np.max(prediction)*100)
    else:
        return ""
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    # Display the resulting frame
    for (x,y,w,h) in faces:
         cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
         roi_gray = gray[y:y+h, x:x+w]
         roi_color = frame[y:y+h, x:x+w]
         arr_f = frame[y:y+h,x:x+w,:]
         img_face = Image.fromarray(convertToRGB(arr_f))
         face_name = getFacePredict(img_face)
         print(face_name)
         font = cv2.FONT_HERSHEY_SIMPLEX
         cv2.putText(frame,face_name,(x,y-2), font, 1,(0,15,250),2,cv2.LINE_AA)
         #eyes = eye_cascade.detectMultiScale(roi_gray)
         #for (ex,ey,ew,eh) in eyes:
             #cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
