Step 1:
	Make sure that you have installed all requirement packages

Step 2: Put all your images into img_train folder
	Name for each person image: "name-class-stt.jpg" == "dang_hoang-02-01.jpg" stt is the number of one person's images.
Step 3: Put some images into img_test folder. Name for each image is the same way.

Step 4: Detect to detect face from image run the facedetection.py file. It will export all faces in train_data folder.
	your job is delete and all mistake images.

Step 5: Make dataset to train your network. run the file name: resize_dataset.py. It will export 2 file: num_train_data.txt and num_train_labels.txt ( num is the number of images) in folder name: dataset

step 6: Make dataset to test your model network. change the status of 
	DATA_PATH = '../test_data'
	DATASET_PATH_DATA = "../dataset/num_test_data.txt"
	DATASET_PATH_LABELS = "../dataset/num_test_labes.txt"
availabe. and disable 
	DATA_PATH = '../train_data'
	DATASET_PATH_DATA = "../dataset/num_train_data.txt"
	DATASET_PATH_LABELS = "../dataset/num_train_labes.txt"

	It will export 2 file: num_test_data.txt and num_test_labels.txt ( num is the number of images) in folder name: dataset

step 7: train your network:
	firstly change ( num_classes = number of people) and human_names = ['first one name','second one name',...]
	
	TRAIN_DATA_PATH = '../dataset/(train data name in folder dataset)'
	TRAIN_LABES_PATH = '../dataset/(train labels name in folder dataset)'
	TEST_DATA_PATH = '../dataset/(test data name in folder dataset)'
	TEST_LABES_PATH = '../dataset/(test labels name in folder dataset)'

when the program finish, you should see the trainning model file in folder model. use it to predict your new photo.

