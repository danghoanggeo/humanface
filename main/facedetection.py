import cv2 as cv
import matplotlib.pyplot as plt
import time
import numpy as np
from PIL import Image
from os import remove, rename, walk, stat
from os.path import getsize, isfile, isdir, join



DATA_PATH = '../temp'
SAVE_PATH = '../face_detected'

def convertToRGB(img):
    return cv.cvtColor(img,cv.COLOR_BGR2RGB)

def showImage(array):
    plt.figure()
    plt.imshow(convertToRGB(array))
    plt.colorbar()
    plt.grid(False)
    plt.show()

def detect_faces(f_cascade,colored_img,fullpath):
    img_copy = colored_img.copy()
    gray = cv.cvtColor(img_copy, cv.COLOR_BGR2GRAY)
    xxx = 1.1
    faces = f_cascade.detectMultiScale(gray, xxx, 5)
    print(faces)
    while len(faces) == 0:
        xxx += 0.1
        faces = f_cascade.detectMultiScale(gray, xxx, 5)
        if xxx >= 2:
            break
        print(faces)

    arr = np.array(img_copy)
    fileName = fullpath.split('/')
    imageName = fileName[len(fileName)-1]
    label = imageName.split('.')[0]
    i = 0
    for (x,y,w,h) in faces:
        cv.rectangle(img_copy,(x,y),(x+w,y+h),(0,255,0),2)
        print("x:%d, x+w:%d, y:%d, y+h: %d"%(x,x+w,y,y+h))
        arr_f = arr[y:y+h,x:x+w,:]
        #showImage(arr_f)
        img_face = Image.fromarray(convertToRGB(arr_f))
        img_face.save("%s/%s+%d.jpg"%(SAVE_PATH,label,i))
        i = i+1
        #roi_gray = gray[y:y+h, x:x+w]
        #roi_color = colored_img[y:y+h, x:x+w]
        #eyes = eye_cascade.detectMultiScale(roi_gray)
        #for (ex,ey,ew,eh) in eyes:
        #    cv.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
    return img_copy

def processdir(path):
    """Recursively processes files in the specified directory matching
    the self.extensions list (case-insensitively)."""

    filecount = 0 # Number of files successfully updated
    fileN = []
    imgArrLs = []
    haar_face_cascade = cv.CascadeClassifier('../data/haarcascade_frontalface_default.xml')

    for root, dirs, files in walk(path):
        for file in files:
            # File has eligible extension, so process
            fullpath = join(root, file)
            imgArr = cv.imread(fullpath)
            gray_img = cv.cvtColor(imgArr, cv.COLOR_BGR2GRAY)
            print(imgArr.shape)
            print(fullpath)
            faces_detect_img = detect_faces(haar_face_cascade,imgArr,fullpath)

#test = cv.imread('human_face/tuyen-vu-03-16.jpg')
#gray_img = cv.cvtColor(test, cv.COLOR_BGR2GRAY)
#if you have matplotlib installed then
#plt.imshow(gray_img, cmap='gray')
#load cascade classifier training file for haarcascade
#haar_face_cascade = cv.CascadeClassifier('data/haarcascade_frontalface_default.xml')
#eye_cascade = cv.CascadeClassifier('data/haarcascade_eye.xml')
#faces_detect_img = detect_faces(haar_face_cascade,eye_cascade,test)

#convert the test image to gray image as opencv face detector expects gray images

#
#plt.show()
if __name__ == "__main__":

    filecount = processdir(DATA_PATH)
